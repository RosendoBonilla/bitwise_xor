﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;

namespace BitwiseXOR
{
    class Program
    {
        static void Main(string[] args) {
            Mat image = Cv2.ImRead(@"..\..\..\Images\image1.jpg");

            #region Copy image

            Mat myCopy = MyCopy(image);
            Mat cvCopy = new Mat();
            image.CopyTo(cvCopy);

            string result = CompareMat(cvCopy, myCopy) ? "equal" : "different";
            Console.WriteLine($"Copied mats are { result }");

            Console.ReadLine();

            #endregion

            #region Translate image

            float[] data = new float[] { 1, 0, 5, 0, 1, 5 };
            Mat matrix = new Mat(2, 3, MatType.CV_32FC1, data);
            Mat cvTranslated = new Mat();
            Cv2.WarpAffine(image, cvTranslated, matrix, new Size(image.Width, image.Height));

            //My method
            Mat myTranslated = MyTranslation(image, 5, 5);

            result = CompareMat(cvTranslated, myTranslated) ? "equal" : "different";
            Console.WriteLine($"Translation mats are { result }");

            Console.ReadLine();
            #endregion

            #region Threshold
            Mat myThresholded = MyThreshold(image, 120);
            Mat cvThresh = new Mat();
            Cv2.Threshold(image, cvThresh, 120, 255, ThresholdTypes.Binary);

            result = CompareMat(myThresholded, cvThresh) ? "equal" : "different";
            Console.WriteLine($"Thresholded mats are { result }");

            Console.ReadLine();
            #endregion

            #region InRange
            Mat myThresholdedRange = MyThresholdRange(image.Clone(), 100, 150);
            Mat cvThreshRange = new Mat();
            // In range execute operation lowerThresh <= P0 && <= upperThresh
            // Our implementation execute operation lowerThresh <= P0 && <= upperThresh
            Cv2.InRange(image.CvtColor(ColorConversionCodes.BGR2GRAY), 101, 149, cvThreshRange);  

            result = CompareMat(myThresholdedRange, cvThreshRange) ? "equal" : "different";
            Console.WriteLine($"Thresholded in range mats are { result }");

            Console.ReadLine();

            #endregion

            #region InRange Inverted
            Mat myThresholdedRangeInv = MyThresholdRangeInverse(image.Clone(), 100, 150, true);
            Mat cvThreshRangeInv = new Mat();
            // In range execute operation lowerThresh <= P0 && <= upperThresh
            // Our implementation execute operation lowerThresh <= P0 && <= upperThresh
            Cv2.InRange(image.CvtColor(ColorConversionCodes.BGR2GRAY), 101, 149, cvThreshRangeInv);
            Cv2.BitwiseNot(cvThreshRangeInv, cvThreshRangeInv);
            
            result = CompareMat(myThresholdedRangeInv, cvThreshRangeInv) ? "equal" : "different";
            Console.WriteLine($"Thresholded in range inverted mats are { result }");

            Console.ReadLine();
            #endregion

            #region InRange Inverted KeepVal
            Mat myThresholdedRangeInvKeep = MyThresholdRangeInverseKeep(image.Clone(), 120, 255, false, -1);
            Mat cvThreshRangeInvKeep = new Mat();

            Cv2.Threshold(image.CvtColor(ColorConversionCodes.BGR2GRAY), cvThreshRangeInvKeep, 120, 255, ThresholdTypes.Tozero);
            /*Cv2.InRange(image.CvtColor(ColorConversionCodes.BGR2GRAY), 101, 149, cvThreshRangeInv);
            Cv2.BitwiseNot(cvThreshRangeInv, cvThreshRangeInv);*/

            result = CompareMat(myThresholdedRangeInvKeep, cvThreshRangeInvKeep) ? "equal" : "different";
            Console.WriteLine($"Thresholded in range inverted keep val mats are { result }");

            Console.ReadLine();
            #endregion

            #region Visualize binary 
            Mat thresh = new Mat();
            Cv2.Threshold(image.Clone().CvtColor(ColorConversionCodes.BGR2GRAY), thresh, 100, 255, ThresholdTypes.Binary);
            Mat myVisualizeBin = VisualizeBinary(thresh);
            Mat cvVisualizeBinv = new Mat();
            Cv2.Threshold(image.CvtColor(ColorConversionCodes.BGR2GRAY), cvVisualizeBinv, 100, 255, ThresholdTypes.BinaryInv);

            result = CompareMat(myThresholdedRangeInv, cvThreshRangeInv) ? "equal" : "different";
            Console.WriteLine($"Thresholded binary inverted mats are { result }");

            Cv2.ImShow("Original", image);
            Cv2.ImShow("My visualize binary", myVisualizeBin);
            Cv2.ImShow("OpenCV thresh", cvVisualizeBinv);
            Cv2.WaitKey();
            Cv2.DestroyAllWindows();
            #endregion

        }

        private static bool CompareMat(Mat mat1, Mat mat2)
        {
            if (mat1.Type() != mat2.Type())
                throw new InvalidOperationException("Mat types are different.");

            Mat dst = new Mat();
            Cv2.BitwiseXor(mat1, mat2, dst);
            Console.WriteLine($"Channels mats are { dst.Channels() }");

            if (dst.Channels() > 1)
                Cv2.CvtColor(dst, dst, ColorConversionCodes.BGR2GRAY);

            return dst.CountNonZero() == 0;
        }

        private static Mat MyCopy(Mat original)
        {
            var matVectOrig = new Mat<Vec3b>(original); // cv::Mat_<cv::Vec3b>
            var indexerOrig = matVectOrig.GetIndexer();

            var copy = new Mat(original.Height, original.Width, original.Type());
            var matVectDst = new Mat<Vec3b>(copy); // cv::Mat_<cv::Vec3b>
            var indexerDst = matVectDst.GetIndexer();

            for (int y = 0; y < original.Height; y++)
            {
                for (int x = 0; x < original.Width; x++)
                {
                    indexerDst[y, x] = indexerOrig[y, x];
                }
            }

            return copy;
        }

        private static Mat MyTranslation(Mat original, float xOffset, float yOffset)
        {
            var matVectOrig = new Mat<Vec3b>(original); // cv::Mat_<cv::Vec3b>
            var indexerOrig = matVectOrig.GetIndexer();

            var translated = new Mat(original.Height, original.Width, original.Type());
            var matVectDst = new Mat<Vec3b>(translated); // cv::Mat_<cv::Vec3b>
            var indexerDst = matVectDst.GetIndexer();

            for (int y = 0; y < original.Height; y++)
            {
                for (int x = 0; x < original.Width; x++)
                {
                    Vec3b origPixel = indexerOrig[y, x];
                    if ((x + xOffset) < original.Width && (y + yOffset) < original.Height)
                        indexerDst[(int)(y + yOffset), (int)(x + xOffset)] = origPixel;
                }
            }

            return translated;
        }

        private static Mat MyThreshold(Mat original, float threshold)
        {
            var matVectOrig = new Mat<Vec3b>(original); // cv::Mat_<cv::Vec3b>
            var indexerOrig = matVectOrig.GetIndexer();

            var thresholded = new Mat(original.Height, original.Width, original.Type());
            var matVectDst = new Mat<Vec3b>(thresholded); // cv::Mat_<cv::Vec3b>
            var indexerDst = matVectDst.GetIndexer();

            for (int y = 0; y < original.Height; y++)
            {
                for (int x = 0; x < original.Width; x++)
                {
                    Vec3b origPixel = indexerOrig[y, x];

                    origPixel.Item0 = origPixel.Item0 > threshold ? (byte)255 : (byte)0;
                    origPixel.Item1= origPixel.Item1 > threshold ? (byte)255 : (byte)0;
                    origPixel.Item2 = origPixel.Item2 > threshold ? (byte)255 : (byte)0;

                    indexerDst[y, x] = origPixel;
                }
            }

            return thresholded;
        }

        private static Mat MyThresholdRange(Mat original, float thresholdLower, float threshholdUpper) {

            Cv2.CvtColor(original, original, ColorConversionCodes.BGR2GRAY);

            var matVectOrig = new Mat<Vec3b>(original); // cv::Mat_<cv::Vec3b>
            var indexerOrig = matVectOrig.GetIndexer();

            var thresholded = new Mat(original.Height, original.Width, original.Type());
            var matVectDst = new Mat<Vec3b>(thresholded); // cv::Mat_<cv::Vec3b>
            var indexerDst = matVectDst.GetIndexer();

            for (int y = 0; y < original.Height; y++) {
                for (int x = 0; x < original.Width; x++) {
                    Vec3b origPixel = indexerOrig[y, x];

                    if ((origPixel.Item0 > thresholdLower) && (origPixel.Item0 < threshholdUpper)) {
                            origPixel.Item0 = 255;
                    } else {
                            origPixel.Item0 = 0;
                    }

                    indexerDst[y, x] = origPixel;
                }  
            }

            return thresholded;
        }

        private static Mat MyThresholdRangeInverse(Mat original, float thresholdLower, float threshholdUpper, bool inverse) {

            Cv2.CvtColor(original, original, ColorConversionCodes.BGR2GRAY);

            var matVectOrig = new Mat<Vec3b>(original); // cv::Mat_<cv::Vec3b>
            var indexerOrig = matVectOrig.GetIndexer();

            var thresholded = new Mat(original.Height, original.Width, original.Type());
            var matVectDst = new Mat<Vec3b>(thresholded); // cv::Mat_<cv::Vec3b>
            var indexerDst = matVectDst.GetIndexer();

            for (int y = 0; y < original.Height; y++) {
                for (int x = 0; x < original.Width; x++) {
                    Vec3b origPixel = indexerOrig[y, x];


                    if((origPixel.Item0 > thresholdLower) && (origPixel.Item0 < threshholdUpper)) {
                        if (inverse)
                            origPixel.Item0 = 0;
                        else
                            origPixel.Item0 = 255;
                    } else {
                        if (inverse)
                            origPixel.Item0 = 255;
                        else
                            origPixel.Item0 = 0;
                    }

                    indexerDst[y, x] = origPixel;
                }
            }

            return thresholded;
        }

        private static Mat MyThresholdRangeInverseKeep(Mat original, float thresholdLower, float threshholdUpper, bool inverse, float keepVal) {

            Cv2.CvtColor(original, original, ColorConversionCodes.BGR2GRAY);

            var matVectOrig = new Mat<Vec3b>(original); // cv::Mat_<cv::Vec3b>
            var indexerOrig = matVectOrig.GetIndexer();

            var thresholded = new Mat(original.Height, original.Width, original.Type());
            var matVectDst = new Mat<Vec3b>(thresholded); // cv::Mat_<cv::Vec3b>
            var indexerDst = matVectDst.GetIndexer();

            for (int y = 0; y < original.Height; y++) {
                for (int x = 0; x < original.Width; x++) {
                    Vec3b origPixel = indexerOrig[y, x];

                    if ((origPixel.Item0 > thresholdLower) && (origPixel.Item0 < threshholdUpper)) {
                        if (inverse) {
                            origPixel.Item0 = 0;
                        } else {
                            if (keepVal < 0)
                                origPixel.Item0 = origPixel.Item0;
                            else
                                origPixel.Item0 = (byte)keepVal;
                        }
                    } else {
                        if (inverse) {
                            if (keepVal < 0)
                                origPixel.Item0 = origPixel.Item0;
                            else
                                origPixel.Item0 = (byte)keepVal;
                        } else {
                            origPixel.Item0 = 0;
                        }
                    }

                    indexerDst[y, x] = origPixel;
                }
            }

            return thresholded;
        }

        private static Mat VisualizeBinary(Mat original) {
            var matVectOrig = new Mat<Vec3b>(original); // cv::Mat_<cv::Vec3b>
            var indexerOrig = matVectOrig.GetIndexer();

            var translated = new Mat(original.Height, original.Width, original.Type());
            var matVectDst = new Mat<Vec3b>(translated); // cv::Mat_<cv::Vec3b>
            var indexerDst = matVectDst.GetIndexer();

            for (int y = 0; y < original.Height; y++) {
                for (int x = 0; x < original.Width; x++) {
                    Vec3b origPixel = indexerOrig[y, x];

                    origPixel.Item0 = (byte)(1 * (255 - origPixel.Item0));
                    indexerDst[y, x] = origPixel;
                }
            }

            return translated;
        }

    }
}
